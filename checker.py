import requests
from random import choice

from femida_checker import Checker, Result


PRIVATE_MESSAGE_FORMATS = [
    'I tell you @{0} that {1}',
    '{1} is important for you @{0} ',
    'Please @{0} {1}',
    'We now that @{0} {1}',
    'Hi, @{0} {1}',
    'Interesting, @{0} {1}',
]

PUBLIC_MESSAGE_FORMATS = [
    ' @all Just talked with my friend @{0} by MMS, awesome!',
    ' @all Used MMS earlier for messages to my bro @{0} ',
    ' @all MMS is the only thing what I use for text my friend @{0} ',
    ' @all MMS is super safe for messaging, my friend @{0} will confirm that',
    ' @all Only one instrument for secret messaging with @{0} ',
]

LOGIN_NAMES = [
    'John',
    'Harry',
    'Oliver',
    'Jack',
    'Charlie',
    'Jacob',
    'Thomas',
    'Alfie',
    'Riley',
    'William',
    'James',
    'Joshua',
    'George',
    'Ethan',
    'Noah',
    'Samuel',
    'Daniel',
    'Oscar',
    'Max',
    'Muhammad',
    'Leo',
    'Tyler',
    'Joseph',
    'Archie',
    'Henry',
    'Lucas'
]

ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"


def generate_login(seed):
    data = seed.decode("hex")
    arr = bytearray(data)
    return "{0}_{1}".format(LOGIN_NAMES[(arr[0] + arr[1]) % 25], seed)


def generate_secondary_login(seed):
    return "{0}_{1}_sec".format(choice(LOGIN_NAMES), seed)


def generate_password(seed):
    chars = []
    data = seed.decode("hex")
    arr = bytearray(data)
    for x in xrange(len(arr)):
        chars.append(ALPHABET[arr[x] % len(ALPHABET)])
    return ''.join(chars)


def sign_in_and_post(endpoint, (user, password), user2, flag):
    try:
        payload = {"username": user2, "password": password}
        requests.post(endpoint + "/login/", data=payload, timeout=10)
        payload = {"username": user, "password": password}
        r = requests.post(endpoint + "/login/", data=payload, timeout=10)
        if r.status_code == 200:
            cookie = r.history[0].cookies["PLAY_SESSION"]
            payload = {"title": "Some secret message",
                       "text": choice(PRIVATE_MESSAGE_FORMATS).format(user2, flag)}
            message_post = requests.post(endpoint + "/post/", cookies=dict(PLAY_SESSION=cookie), data=payload)
            if message_post.status_code == 200:
                payload = {"title": "Some public message",
                           "text": choice(PUBLIC_MESSAGE_FORMATS).format(user2)}
                message_post = requests.post(endpoint + "/post/", cookies=dict(PLAY_SESSION=cookie), data=payload)
                if message_post.status_code == 200:
                    return Result.OK
                else:
                    return Result.MUMBLE
            else:
                return Result.MUMBLE
        else:
            return Result.MUMBLE
    except requests.ConnectionError:
        return Result.DOWN
    except requests.HTTPError:
        return Result.DOWN
    except requests.exceptions.Timeout:
        return Result.DOWN


def sign_in_and_check(endpoint, (user, password), flag):
    try:
        payload = {"username": user, "password": password}
        r = requests.post(endpoint + "/login/", data=payload, timeout=10)
        if r.status_code == 200:
            cookie = r.history[0].cookies["PLAY_SESSION"]
            my = requests.get(endpoint + "/my/", cookies=dict(PLAY_SESSION=cookie))
            if my.status_code == 200:
                text = my.text
                if not flag in text:
                    return Result.CORRUPT
                else:
                    return Result.OK
            else:
                return Result.MUMBLE
        else:
            return Result.MUMBLE
    except requests.ConnectionError:
        return Result.DOWN
    except requests.HTTPError:
        return Result.DOWN
    except requests.exceptions.Timeout:
        return Result.DOWN


class MMSChecker(Checker):
    def _push(self, endpoint, flag_id, flag):
        user1 = (generate_login(flag_id), generate_password(flag_id))
        user2 = (generate_secondary_login(flag_id), generate_password(flag_id))
        result = sign_in_and_post("http://{0}".format(endpoint), user1, user2[0], flag)
        return result

    def _pull(self, endpoint, flag_id, flag):
        login_pair = (generate_login(flag_id), generate_password(flag_id))
        result = sign_in_and_check("http://{0}".format(endpoint), login_pair, flag)
        return result


checker = MMSChecker()
checker.run()